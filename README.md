# Dit is een repo waarbij de Docker gedeelte wordt getest op een proxmox VM.

## benodigdheden:
- Git
- Linux machine 

### Ansible-Vault

De vault is niet geinclude in de repo, wegens veiligheidsredenen. Deze moet je zelf aanmaken.

ansible-vault create "naam".yml

Hierbij moet je een wachtwoord aanmaken, deze moet je onthouden. Als je het wachtwoord bent vergeten, kan je het niet meer decrypten.
Vervolgens zit je in Vi(m) en kan je de passwords variabelen erin zetten.
VIM sluit je doormiddel van ":wq" in te typen. Dit staat voor write en quit.

### Gitlab
Gitlab draait op poort 8081, dit kan je aanpassen in de docker-compose.yml file.

### Nextcloud
Nextcloud draait op poort 8080, dit kan je aanpassen in de docker-compose.yml file.